#ifndef DIGTALCLOCK_H
#define DIGTALCLOCK_H

#include <QWidget>

namespace Ui {
class DigtalClock;
}

class DigtalClock : public QWidget
{
    Q_OBJECT

public:
    explicit DigtalClock(QWidget *parent = nullptr);
    ~DigtalClock();

private:
    Ui::DigtalClock *ui;
};

#endif // DIGTALCLOCK_H
