#include "digtalclock.h"
#include "ui_digtalclock.h"

DigtalClock::DigtalClock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DigtalClock)
{
    ui->setupUi(this);
}

DigtalClock::~DigtalClock()
{
    delete ui;
}
