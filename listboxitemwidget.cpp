#include "listboxitemwidget.h"
#include "ui_listboxitemwidget.h"

ListBoxItemWidget::ListBoxItemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ListBoxItemWidget)
{
    ui->setupUi(this);

    connect(ui->cbEvent, SIGNAL(currentTextChanged(QString)), SLOT(eventChanged()));
}

ListBoxItemWidget::~ListBoxItemWidget()
{
    delete ui;
}

void
ListBoxItemWidget::eventChanged()
{
    if (ui->cbEvent->currentIndex() > 1)
    {
        ui->tmEvent->hide();
    }
    else
    {
        ui->tmEvent->show();
    }
}
