#ifndef LISTBOXITEMWIDGET_H
#define LISTBOXITEMWIDGET_H

#include <QWidget>

namespace Ui {
class ListBoxItemWidget;
}

class ListBoxItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ListBoxItemWidget(QWidget *parent = nullptr);
    ~ListBoxItemWidget();

private:
    Ui::ListBoxItemWidget *ui;

public slots:
    void eventChanged();
};

#endif // LISTBOXITEMWIDGET_H
