#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QListWidgetItem>
#include "listboxitemwidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    connect(ui->btnAddRule, SIGNAL(clicked()), SLOT(addItem()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void
MainWindow::addItem()
{
    QListWidgetItem *item = new QListWidgetItem();

    ui->ruleView->addItem(item);

    ListBoxItemWidget *row = new ListBoxItemWidget();
    item->setSizeHint(row->minimumSizeHint());

    ui->ruleView->setItemWidget(item, row);
}

